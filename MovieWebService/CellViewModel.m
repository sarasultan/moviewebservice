//
//  CellViewModel.m
//  MovieWebService
//
//  Created by Sara Sultan on 3/3/20.
//  Copyright © 2020 TestCompany. All rights reserved.
//

#import "CellViewModel.h"

@implementation CellViewModel
-(id)initWithReleaseDate:(NSString *)releaseDate title:(NSString *)title filmRating:(NSString *)filmRating rating:(NSString *)rating{
    self = [super init];
    if (self) {
        self.releaseDate = releaseDate;
        self.title = title;
        self.filmRating = filmRating;
        self.rating = rating;
    }
    return self;
}
@end
