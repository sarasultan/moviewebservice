//
//  CellViewModel.h
//  MovieWebService
//
//  Created by Sara Sultan on 3/3/20.
//  Copyright © 2020 TestCompany. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellViewModel : NSObject
@property(nonatomic,strong) NSString *releaseDate;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *filmRating;
@property(nonatomic,strong) NSString *rating;

-(id)initWithReleaseDate:(NSString *)releaseDate title:(NSString *)title filmRating:(NSString *)filmRating rating:(NSString *)rating;
@end

NS_ASSUME_NONNULL_END
