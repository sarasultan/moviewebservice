//
//  MoviesListRouter.m
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import "MoviesListRouter.h"
#import <UIKit/UIKit.h>
#import "MoviesListViewController.h"
#import "MoviesListPresenter.h"
#import "MoviesListInteractor.h"
#import "MovieWebService-Swift.h"

@interface MoviesListRouter()
@property (nonatomic, weak) UIViewController *view;
@end
@implementation MoviesListRouter

#pragma mark - MoviesListRouterInput
-(id) init: (UIViewController *)viewController {
    self = [super init];
    if (self) {
        self.view = viewController;
    }
    return self;
}
+(UIViewController *) buildModule {
    MoviesListViewController *view = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateInitialViewController];
    MoviesListPresenter *presenter = [[MoviesListPresenter alloc] init];
    MoviesListInteractor *interactor = [[MoviesListInteractor alloc] init];
    MoviesListRouter *router = [[MoviesListRouter alloc] init:view];
    
    view.presenter = presenter;
    
    presenter.view = view;
    presenter.interactor = interactor;
    presenter.router = router;
    
    interactor.presenter = presenter;
    
    return view;
}
-(void)goToDetailsView:(Film *)film {
    DetailsViewController *detailsViewController = (DetailsViewController *)[DetailsRouter buildWith:film];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] init];
    backItem.title = @"Back";
    self.view.navigationItem.backBarButtonItem = backItem;
    [self.view.navigationController pushViewController:detailsViewController animated:true];
}

@end
