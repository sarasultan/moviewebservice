//
//  MoviesListInteractor.h
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import "MoviesListPresenterToInteractorProtocol.h"
#import "MoviesListInteractorToPresenterProtocol.h"

@interface MoviesListInteractor : NSObject <MoviesListPresenterToInteractorProtocol>

@property (nonatomic, weak) id<MoviesListInteractorToPresenterProtocol> presenter;

@end
