//
//  MoviesListPresenterToViewProtocol.h
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CellViewModel.h"

@protocol MoviesListPresenterToViewProtocol <NSObject>
- (void)setData:(NSArray<CellViewModel *> *)data;
- (void)showError:(NSString *)errorMessage;
@end
