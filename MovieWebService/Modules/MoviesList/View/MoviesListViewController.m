//
//  MoviesListViewController.m
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import "MoviesListViewController.h"
#import "CellTableViewCell.h"

@implementation MoviesListViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
	[super viewDidLoad];
    [self initUI];
    [self setupInitialState];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.presenter fetchData];
}

-(void) initUI {
    tableView.delegate = self;
    tableView.dataSource = self;
    
    [tableView registerNib:[UINib nibWithNibName:@"CellTableViewCell" bundle:nil] forCellReuseIdentifier:@"CellTableViewCell"];
}
- (void)setupInitialState {
    self.navigationItem.title = @"RootViewController";
    self.view.backgroundColor = [UIColor whiteColor];
}

#pragma mark - MoviesListPresenterToViewProtocol

-(void)setData:(NSArray<CellViewModel *> *)data{
    cellViewModels = data;
    [tableView reloadData];
}

-(void)showError:(NSString *)errorMessage{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil] ;
    [alert addAction:alertAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return cellViewModels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CellTableViewCell";
    CellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    CellViewModel *cellViewModel = [cellViewModels objectAtIndex:indexPath.row];
    cell.name.text = cellViewModel.title;
    cell.date.text = cellViewModel.releaseDate;
    cell.filmRating.text = cellViewModel.filmRating;
    cell.rating.text = cellViewModel.rating;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.presenter didSelectFilmAt:indexPath.row];
}

@end
