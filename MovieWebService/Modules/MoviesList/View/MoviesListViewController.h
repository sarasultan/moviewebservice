//
//  MoviesListViewController.h
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MoviesListViewToPresenterProtocol.h"
#import "MoviesListPresenterToViewProtocol.h"
#import "CellViewModel.h"

@protocol MoviesListViewOutput;

@interface MoviesListViewController : UIViewController <MoviesListPresenterToViewProtocol,UITableViewDelegate,UITableViewDataSource> {
    NSArray<CellViewModel *> *cellViewModels;
    IBOutlet  UITableView *tableView;
}

@property (nonatomic, strong) id<MoviesListViewToPresenterProtocol> presenter;

@end
