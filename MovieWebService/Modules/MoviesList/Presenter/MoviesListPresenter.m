//
//  MoviesListPresenter.m
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import "MoviesListPresenter.h"
#import "CellViewModel.h"

@implementation MoviesListPresenter

#pragma mark - MoviesListViewToPresenterProtocol

-(void)fetchData{
    [self.interactor getFilm];
}
-(void)didSelectFilmAt:(NSInteger)index{
    Film *selectedFilm = [Films objectAtIndex:index];
    [self.router goToDetailsView:selectedFilm];
}
#pragma mark - MoviesListInteractorToPresenterProtocol
-(void)setData:(Film *)film{
    Films = [NSMutableArray arrayWithObject:film];
    
    NSCalendar* cal = [NSCalendar new];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, yyyy"];
    [formatter setCalendar:cal];
    NSString *dateText = [formatter stringFromDate:film.releaseDate];


    NSString *filmRatingText = @"";
    switch (film.filmRating) {
        case G:
            filmRatingText = @"G";
            break;
        case PG:
            filmRatingText = @"PG";
            break;
        case PG13:
            filmRatingText = @"PG13";
            break;
        case R:
            filmRatingText = @"R";
            break;
        default:
            filmRatingText = @"";
            break;
    }
    NSString *ratingText = [[NSNumber numberWithFloat:film.rating] stringValue];
    CellViewModel *cellViewModel = [[CellViewModel alloc] initWithReleaseDate:dateText title:film.name filmRating:filmRatingText rating:ratingText];
    
    [self.view setData:[NSArray arrayWithObject:cellViewModel]];
}
-(void)failedFetchFilm{
    [self.view showError:@"Error fetching data, please try again later"];
}
@end
