//
//  MoviesListPresenter.h
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

#import "MoviesListViewToPresenterProtocol.h"
#import "MoviesListPresenterToViewProtocol.h"
#import "MoviesListPresenterToInteractorProtocol.h"
#import "MoviesListPresenterToRouterProtocol.h"
#import "MoviesListInteractorToPresenterProtocol.h"

@interface MoviesListPresenter : NSObject <MoviesListViewToPresenterProtocol, MoviesListInteractorToPresenterProtocol>{
    NSMutableArray *Films;
}

@property (nonatomic, weak) id<MoviesListPresenterToViewProtocol> view;
@property (nonatomic, strong) id<MoviesListPresenterToInteractorProtocol> interactor;
@property (nonatomic, strong) id<MoviesListPresenterToRouterProtocol> router;

@end
