//
//  DetailsRouter.swift
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

@objcMembers public class DetailsRouter:NSObject, DetailsPresenterToRouterProtocol {
	
    private unowned let viewController: UIViewController
    private init(viewController: UIViewController) {
        self.viewController = viewController
    }
    // MARK: - DetailsPresenterToRouterProtocol

    static func build(with film: Film) -> UIViewController {
        let viewController = DetailsViewController()
        let router = DetailsRouter(viewController: viewController)
        let presenter = DetailsPresenter()
        let interactor = DetailsInteractor()

        interactor.presenter = presenter

        presenter.view = viewController
        presenter.router = router
        presenter.interactor = interactor

        viewController.presenter = presenter

        presenter.film = film

        return viewController
    }
}
