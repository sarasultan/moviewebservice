//
//  DetailsViewController.swift
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

import UIKit
import SnapKit

class DetailsViewController: UIViewController, DetailsPresenterToViewProtocol, TappableLabelDelegate {

    var presenter: DetailsViewToPresenterProtocol?

    var directorName: UILabel!
    var directorNameValue: UILabel!
    var tapToShowMore: TappableLabel!
    var actorName: UILabel!
    var actorScreenName: UILabel!
    var actorNameValue: UILabel!
    var actorScreenNameValue: UILabel!
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.responds(to: #selector(getter: UIViewController.edgesForExtendedLayout)){
            self.edgesForExtendedLayout = []
        }
        self.navigationController?.navigationBar.isTranslucent = false
       
        
        initUI()
        presenter?.viewIsReady()
    }
    func initUI(){
        self.view.backgroundColor = .white
        
        directorName = UILabel()
        self.view.addSubview(directorName)
        directorName.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.top.equalToSuperview().inset(20)
            make.left.right.equalToSuperview().inset(20)
        }
        directorName.text = "Director Name"
        
        directorNameValue = UILabel()
        self.view.addSubview(directorNameValue)
        directorNameValue.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.left.right.equalToSuperview().inset(20)
            make.top.equalTo(directorName.snp.bottom).offset(5)
        }

        tapToShowMore = TappableLabel()
        self.view.addSubview(tapToShowMore)
        tapToShowMore.text = "Tap here to show more"
        tapToShowMore.delegate = self
        tapToShowMore.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.left.right.equalToSuperview().inset(20)
            make.top.equalTo(directorNameValue.snp.bottom).offset(20)
        }

        actorName = UILabel()
        self.view.addSubview(actorName)
        actorName.text = "Actor Name"
        actorName.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.left.right.equalToSuperview().inset(20)
            make.top.equalTo(tapToShowMore.snp.bottom).offset(20)
            make.top.equalTo(directorNameValue.snp.bottom).offset(20).priority(.low)
        }
        
        actorNameValue = UILabel()
        self.view.addSubview(actorNameValue)
        actorNameValue.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.left.right.equalToSuperview().inset(20)
            make.top.equalTo(actorName.snp.bottom).offset(5)
        }
        
        actorScreenName = UILabel()
        self.view.addSubview(actorScreenName)
        actorScreenName.text = "Actor Screen Name"
        actorScreenName.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.left.right.equalToSuperview().inset(20)
            make.top.equalTo(actorNameValue.snp.bottom).offset(20)
        }
        
        actorScreenNameValue = UILabel()
        self.view.addSubview(actorScreenNameValue)
        actorScreenNameValue.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.left.right.equalToSuperview().inset(20)
            make.top.equalTo(actorScreenName.snp.bottom).offset(5)
        }
        
        actorName.isHidden = true
        actorScreenName.isHidden = true
        actorNameValue.isHidden = true
        actorScreenNameValue.isHidden = true
        
        directorNameValue.font = UIFont(name: directorNameValue.font.fontName, size: 14)
        actorScreenNameValue.font = directorNameValue.font
        actorNameValue.font = directorNameValue.font
    }

    
    // MARK: DetailsPresenterToViewProtocol
    func showFilmInfo(directorName: String, actorName: String, actorScreenName: String) {
        self.directorNameValue.text = directorName
        self.actorNameValue.text = actorName
        self.actorScreenNameValue.text = actorScreenName
    }
    
    func didReceiveTouch() {
        tapToShowMore.removeFromSuperview()
        
        actorName.isHidden = false
        actorScreenName.isHidden = false
        actorNameValue.isHidden = false
        actorScreenNameValue.isHidden = false
    }
    
//    deinit {
//        self.directorName = nil
//        self.directorNameValue = nil
//        self.tapToShowMore = nil
//        self.actorName = nil
//        self.actorScreenName = nil
//        self.actorNameValue = nil
//        self.actorScreenNameValue = nil
//    }
}
