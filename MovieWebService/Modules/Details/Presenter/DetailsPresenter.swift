//
//  DetailsPresenter.swift
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

class DetailsPresenter: DetailsViewToPresenterProtocol, DetailsInteractorToPresenterProtocol {
    
    weak var view: DetailsPresenterToViewProtocol?
    var interactor: DetailsPresenterToInteractorProtocol?
    var router: DetailsPresenterToRouterProtocol?
    
    public var film: Film?
    
    // MARK: - DetailsViewToPresenterProtocol
    func viewIsReady() {
        if film?.cast.count ?? 0 > 0 {
            if let actor: Actor = film?.cast[0] {
                
                let directorName = film?.director.name ?? ""
                let actorName = actor.name ?? ""
                let actorScreenName = actor.screenName ?? ""
                
                self.view?.showFilmInfo(directorName: directorName, actorName: actorName, actorScreenName: actorScreenName)
            }
        }
    }
}
