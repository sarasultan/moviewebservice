//
//  DetailsPresenterToViewProtocol.swift
//  MovieWebService
//
//  Created by testDev on 11/04/2017.
//  Copyright © 2017 Agoda Services Co. Ltd. All rights reserved.
//

protocol DetailsPresenterToViewProtocol: class {
    func showFilmInfo(directorName : String , actorName : String, actorScreenName : String);
}
