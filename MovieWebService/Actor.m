//
//  Actor.m
//  MovieWebService
//
//  Created by testDev on 4/11/17.
//  Copyright © 2017 TestCompany. All rights reserved.
//

#import "Actor.h"

@implementation Actor

- (id)initWithData:(NSDictionary *)data {
    self = [super initWithData:data];
    if (self) {
        self.screenName = [data objectForKey:@"screenName"];
    }
    return self;
}
- (NSMutableArray *)dataWithArray:(NSArray *)array {
    NSMutableArray *result = [[NSMutableArray alloc] initWithCapacity:array.count];
    for (NSDictionary * element in array) {
        [result addObject:[[Actor alloc]initWithData:element]];
    }
    return result;
}

@end
